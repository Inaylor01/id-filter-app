/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.igb.idfilter;

import aQute.bnd.annotation.component.Component;
import com.affymetrix.genometry.BioSeq;
import com.affymetrix.genometry.Scored;
import com.affymetrix.genometry.filter.SymmetryFilterI;
import com.affymetrix.genometry.general.BoundedParameter;
import com.affymetrix.genometry.general.IParameters;
import com.affymetrix.genometry.general.Parameter;
import com.affymetrix.genometry.general.Parameters;
import com.affymetrix.genometry.operator.comparator.MathComparisonOperator;
import com.affymetrix.genometry.parsers.FileTypeCategory;
import com.affymetrix.genometry.symmetry.impl.SeqSymmetry;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author noorzahara
 * @author chesterdias
 * @author irvinnaylor
 * 
 */
@Component(immediate = true)
public class IDFilter implements SymmetryFilterI, IParameters {

    private final static String ID = "ID";
    private final static String COMPARATOR = "comparator";
    private final static String DEFAULT_ID = "None";
    protected Parameters parameters;

    private final Parameter<String> id = new Parameter<>(DEFAULT_ID);
    private final static String COMPARATOR_VALUE = "Like";

    public IDFilter() {
        super();
        parameters = new Parameters();
        parameters.addParameter(ID, String.class, id);
    }

    @Override
    public boolean filterSymmetry(BioSeq seq, SeqSymmetry sym) {
        String filterId = id.get();
        String testId = sym.getID();
         if (testId.contains(filterId)||testId.equals(filterId)) {
             return true;
         }
        return false;
    }

    @Override
    public String getName() {
        return ID;
    }

    @Override
    public String getPrintableString() {
        return ID + " " + COMPARATOR_VALUE + " " + id.get();
    }

    @Override
    public String getDisplay() {
        return ID;
    }

    @Override
    public boolean isFileTypeCategorySupported(FileTypeCategory fileTypeCategory) {
        return fileTypeCategory == FileTypeCategory.Annotation
                || fileTypeCategory == FileTypeCategory.Alignment
                || fileTypeCategory == FileTypeCategory.ProbeSet;
    }

    @Override
    public SymmetryFilterI newInstance() {
        try {
            SymmetryFilterI newInstance = getClass().getConstructor().newInstance();
            if (newInstance instanceof IParameters) {
                getParametersType().keySet().forEach((key) -> {
                    ((IParameters) newInstance).setParameterValue(key, getParameterValue(key));
                });
            }
            return newInstance;
        } catch (Exception ex) {
        }
        return null;
    }

    @Override
    public Map<String, Class<?>> getParametersType() {
        return parameters.getParametersType();
    }

    @Override
    public boolean setParametersValue(Map<String, Object> params) {
        return parameters.setParametersValue(params);
    }

    @Override
    public boolean setParameterValue(String key, Object value) {
        return parameters.setParameterValue(key, value);
    }

    @Override
    public Object getParameterValue(String key) {
        return parameters.getParameterValue(key);
    }

    @Override
    public List<Object> getParametersPossibleValues(String key) {
        return parameters.getParametersPossibleValues(key);
    }

}
