This app allows you to filter items in a track by their ID, only showing items that match an ID string you specify. 

To run the App:

1. Install the App (see Readme.md for more details)

2. Right click an alignment track from a loaded BAM file

3. Select "Filter"

4. Select "Add"

5. In the Show Only drop-down menu, select ID.
    
From there you can enter your desired ID String in the text field

Note: Wildcard characters are not supported at this time, the whole string must be entered.