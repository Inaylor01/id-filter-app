# About #

This repository contins an IGB app that allows a user to filter items in an alignment track by their ID.

### How to run this IGB App ###

There are two ways to try it out!

* * *

## Option 1: Build it on your local computer.

* Clone the repository
* Build the App by running `mvn package`
* Start IGB 9.1 or higher 
* Select **Open App Manager** from the IGB **Tools** menu
* Click **Manage Repositories** button
* Use the file chooser to select the "target" directory in the cloned repository as a new App repository
* Return to IGB App Manager. You should now see a new App named **ID Filter** in the list of available Apps.
* Click **Install** to install the App

Once the App is installed, here is how to run it:

* Install the App (see Readme.md for more details)
* Right click an alignment track from a loaded BAM file
* Select "Filter"
* Select "Add"
* In the Show Only drop-down menu, select ID

## Option 2: Use this repository's Downloads folder as an IGB App repository

Because this repository's **Downloads** folder contains an OBR index file (repository.xml), you can try out the App using the **Downloads** folder as an App Repository.

To do this, follow the instructions above, but instead of selecting a local folder, enter the URL of the **Downloads** folder.

